/*
 * Clase Socio
 */
package gimnasio;

import java.io.Serializable;

/**
 *
 * @author mfontana
 */
public class Socio implements Serializable {
    
    private int numSocio;
    private String nombre;
    private String telefono;
    private double cuota;
    private boolean vip;

    public Socio() {
    }

    public Socio(int numSocio, String nombre, String telefono, double cuota, boolean vip) {
        this.numSocio = numSocio;
        this.nombre = nombre;
        this.telefono = telefono;
        this.cuota = cuota;
        this.vip = vip;
    }

    @Override
    public String toString() {
        String texto = "Nº: "+ numSocio + ", nombre=" + nombre + ", telefono=" + telefono + ", cuota=" + cuota;
        if (vip) {
            texto += " *VIP*";
        } else {
            texto += " no VIP";
        }
        return texto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.numSocio;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Socio other = (Socio) obj;
        if (this.numSocio != other.numSocio) {
            return false;
        }
        return true;
    }
    
    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }


    public double getCuota() {
        return cuota;
    }

    public void setCuota(double cuota) {
        this.cuota = cuota;
    }


    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public int getNumSocio() {
        return numSocio;
    }

    public void setNumSocio(int numSocio) {
        this.numSocio = numSocio;
    }

}
