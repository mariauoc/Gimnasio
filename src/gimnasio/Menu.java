/*
 * Interacción con el usuario
 */
package gimnasio;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import ocutilidades.EntradaDatos;
import ocutilidades.Fichero;

/**
 *
 * @author mfontana
 */
public class Menu {

    private Fichero socioFile;
    private ListaSocios misSocios;
    private Fichero actividadFile;
    private ListaActividades misActividades;

    public Menu() {
        // Sólo empezar cargamos nuestra lista socios del fichero y si no hay 
        // inicializamos la lista como una vacía
        socioFile = new Fichero("socios.xml");
        actividadFile = new Fichero("actividades.xml");
        misSocios = (ListaSocios) socioFile.leer();
        if (misSocios == null) {
            misSocios = new ListaSocios();
        }
        misActividades = (ListaActividades) actividadFile.leer();
        if (misActividades == null) {
            misActividades = new ListaActividades();
        }
        int opcion;
        do {
            mostrarMenu();
            opcion = EntradaDatos.pedirEntero("Escoge una opcion");
            switch (opcion) {
                case 1: // alta socio
                    nuevoSocio();
                    break;
                case 2: // Para vosotros alta activitat
                    nuevaActividad();
                    break;
                case 3:
                    consultas();
                    break;
                case 4:
                    inscribirSocioActividad();
                case 5:
                    modificarVIP();
                case 0:
                    System.out.println("Haz mucho ejercicio!");
                    break;
                default:
                    System.out.println("Opción incorrecta");
            }
        } while (opcion != 0);

    }
    
    private void modificarVIP() {
        int nsocio = EntradaDatos.pedirEntero("Introduce nº de socio");
        Socio s = misSocios.getSocio(nsocio);
        if (s == null) {
            JOptionPane.showMessageDialog(null, "Socio no existe", "Socio incorrecto", JOptionPane.ERROR_MESSAGE);
        } else {
            if (s.isVip()) {
                JOptionPane.showMessageDialog(null, "El socio es VIP");
            } else {
                JOptionPane.showMessageDialog(null, "El socio no es VIP");
            }
            int respuesta = JOptionPane.showConfirmDialog(null, "¿Quieres cambiar el estado de VIP del socio?", "Alta / Baja VIP", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                // Cambiamos el estado que tenía en el VIP
                // Si era false ahora es true y vicecersa
                s.setVip(!s.isVip());
                socioFile.grabar(misSocios);
                JOptionPane.showMessageDialog(null, "Estado cambiado.");
            }
        }
    }

    private void inscribirSocioActividad() {
        int nsocio = EntradaDatos.pedirEntero("Nº del socio que desea inscribirse");
        Socio s = new Socio();
        s.setNumSocio(nsocio);
        if (misSocios.existe(s)) {
            String nombre = EntradaDatos.pedirCadena("Nombre de la actividad en la que se desea inscribir");
            Actividad a = new Actividad();
            a.setNombre(nombre);
            if (misActividades.existe(a)) {
                // Leo la actividad correspondiente al nombre introducido
                a = misActividades.getActividad(nombre);
                if (a.getPlazasDisponibles() > 0) {
                    s = misSocios.getSocio(nsocio);
                    // Inscribimos al socio en la actividad
                    // Incrementamos el nº de inscritos en la actividad
                    a.setInscritos(a.getInscritos() + 1);
                    // Incrementamos la cuota mensual del socio con el precio de la actividad
                    s.setCuota(s.getCuota() + a.getPrecio());
                    actividadFile.grabar(misActividades);
                    socioFile.grabar(misSocios);
                    JOptionPane.showMessageDialog(null, "Socio " + s.getNombre() + " inscrito a la actividad " + a.getNombre());
                } else {
                    JOptionPane.showMessageDialog(null, "Lo siento, esa actividad está full");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No existe actividad con ese nombre", "Actividad inexistente", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No existe ningún socio con ese nº", "Socio inexistente", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void consultas() {
        int opcion;
        do {
            menuConsultas();
            opcion = EntradaDatos.pedirEntero("Escoge una opción");
            switch (opcion) {
                case 1:
                    listadoSocios();
                    break;
                case 2:
                    listadoActividades();
                case 3:
                    JOptionPane.showMessageDialog(null, "El total que recibe el gimnasio por sus socios es: " + misSocios.cuotasTotales() + "€");
                case 4:
                    actividadesDisponibles();
                case 5:
                    System.out.println("Volviendo al menú principal...");
                    break;
                default:
                    System.out.println("Opción incorrecta.");
            }
        } while (opcion != 5);
    }

    private void actividadesDisponibles() {
        ArrayList<Actividad> disponibles = misActividades.actividadesDisponibles();
        if (disponibles.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No quedan actividades con plazas disponibles");
        } else {
            System.out.println("***** ACTIVIDADES CON PLAZAS DISPONIBLES *****");
            System.out.println("Nombre\t\tPlazas disponibles");
            for (Actividad a : disponibles) {
                System.out.println(a.getNombre() + "\t\t" + a.getPlazasDisponibles());
            }
        }
    }

    private void listadoActividades() {
        int cantidad = misActividades.cantidad();
        if (cantidad == 0) {
            System.out.println("No hay actividades en el sistema");
        } else {
            System.out.println("***** LISTADO DE ACTIVIDADES *****");
            for (int i = 0; i < cantidad; i++) {
                Actividad a = misActividades.getActividadPosicion(i);
                System.out.println(a);
            }
        }
    }

    private void listadoSocios() {
        int cantidad = misSocios.cantidad();
        if (cantidad == 0) {
            System.out.println("No hay socios en el sistema.");
        } else {
            System.out.println("***** LISTADO DE SOCIOS *****");
            for (int i = 0; i < cantidad; i++) {
                Socio s = misSocios.getSocioPosicion(i);
                System.out.println(s);
            }
        }
    }

    private void menuConsultas() {
        System.out.println("Informes");
        System.out.println("1. Listado de socios");
        System.out.println("2. Listado de actividades");
        System.out.println("3. Ingresos mensuales");
        System.out.println("4. Actividades con plazas disponibles.");
        System.out.println("5. Volver al menú principal.");
    }

    private void nuevaActividad() {
        boolean existe = false;
        String nombre;
        do {
            nombre = EntradaDatos.pedirCadena("Nombre de la actividad");
            Actividad a = new Actividad();
            a.setNombre(nombre);
            existe = misActividades.existe(a);
            if (existe) {
                JOptionPane.showMessageDialog(null, "Ya existe una actividad con ese nombre", "Actividad repetida", JOptionPane.ERROR_MESSAGE);
            }
        } while (existe);
        int maxPlazas;
        do {
            maxPlazas = EntradaDatos.pedirEntero("Nº máximo de plazas");
            if (maxPlazas < 1) {
                JOptionPane.showMessageDialog(null, "Al menos debería poderse inscribir un socio, ¿no?", "Valor incorrecto", JOptionPane.ERROR_MESSAGE);
            }
        } while (maxPlazas < 1);
        double precio;
        do {
            precio = EntradaDatos.pedirDouble("Precio mensual de la actividad");
            if (precio <= 0) {
                JOptionPane.showMessageDialog(null, "Si hombre! Actividades gratis o pagando por darlas!", "Valor incorrecto", JOptionPane.ERROR_MESSAGE);
            }
        } while (precio <= 0);
        Actividad a = new Actividad(nombre, maxPlazas, precio);
        misActividades.altaActividad(a);
        actividadFile.grabar(misActividades);
        JOptionPane.showMessageDialog(null, "Actividad dada de alta.");
    }

    private void nuevoSocio() {
        int nsocio;
        boolean existe = false;
        do {
            nsocio = EntradaDatos.pedirEntero("Nº de socio");
            if (nsocio <= 0) {
                System.out.println("El nº no puede ser menor o igual a cero.");
            } else {
                Socio s = new Socio();
                s.setNumSocio(nsocio);
                existe = misSocios.existe(s);
                if (existe) {
                    //System.out.println("Ya existe un socio con ese número");
                    JOptionPane.showMessageDialog(null, "Ya existe un socio con ese número", "Nº de socio duplicado", JOptionPane.ERROR_MESSAGE);
                }
            }
        } while (nsocio <= 0 || existe);
        String nombre = EntradaDatos.pedirCadena("Nombre del socio");
        String telefono = EntradaDatos.pedirCadena("Teléfono");
        double cuota;
        do {
            cuota = EntradaDatos.pedirDouble("Cuota mensual");
            if (cuota < 10) {
                System.out.println("La cuota mínima es de 10 euros.");
            }
        } while (cuota < 10);
        String respuesta;
        boolean vip = false;
        do {
            respuesta = EntradaDatos.pedirCadena("¿Es socio VIP (S/N)?");
            if (respuesta.equalsIgnoreCase("s")) {
                vip = true;
            } else if (respuesta.equalsIgnoreCase("n")) {
                vip = false;
            } else {
                System.out.println("Respuesta incorrecta. Debes indicar s o n");
            }
        } while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
        Socio s = new Socio(nsocio, nombre, telefono, cuota, vip);
        misSocios.altaSocio(s);
        socioFile.grabar(misSocios);
//        System.out.println("Socio dado de alta.");
        JOptionPane.showMessageDialog(null, "Socio dado de alta.");
    }

    private void mostrarMenu() {
        System.out.println("Gimnasio NoTeRompas");
        System.out.println("1. Nuevo Socio");
        System.out.println("2. Nueva Actividad");
        System.out.println("3. Consultas");
        System.out.println("4. Inscribir socio a una actividad");
        System.out.println("5. Alta / Baja VIP");
        System.out.println("0. Salir");
    }

}
