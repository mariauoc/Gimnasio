/*
 * Clase que encapsula el ArrayList de Actividad
 */
package gimnasio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class ListaActividades implements Serializable {

    private ArrayList<Actividad> lista;

    public ListaActividades() {
        lista = new ArrayList<>();
    }
    
    // Método que añade una Actividad a la lista
    public void altaActividad(Actividad a) {
        lista.add(a);
    }
    
    // Método que indica si una Actividad está en la lista o no
    // Dos actividades son iguales si tienen el mismo nombre (utiliza equals)
    public boolean existe(Actividad a) {
        return lista.contains(a);
    }
    
    // Método que indica si una Actividad está en la lista a partir del nombre.
    public boolean existe(String nombre) {
        for (Actividad a : lista) {
            if (a.getNombre().equalsIgnoreCase(nombre)) {
                return true;
            }
        }
        return false;
    }
    
    // Método que devuelve la cantidad de Actividades que hay en la lista
    public int cantidad() {
        return lista.size();
    }
    
    // Método que a partir del nombre nos devuelve la Actividad
    // Si no existe devuelve null
    public Actividad getActividad(String nombre) {
        for (Actividad a : lista) {
            if (a.getNombre().equalsIgnoreCase(nombre)) {
                return a;
            }
        }
        return null;
    }
    
    // Método qeu devuelve una Actividad a partir de su posición en el ArrayList
    public Actividad getActividadPosicion(int posicion) {
        return lista.get(posicion);
    }
    
    // Método que devuelve un ArrayList con las actividades
    // que tienen plazas disponibles
    public ArrayList<Actividad> actividadesDisponibles() {
        ArrayList<Actividad> disponibles = new ArrayList<>();
        for (Actividad a : lista) {
            if (!a.isFull()) {  // Si no (!) está llena se añade a la lista
                disponibles.add(a);
            }
        }
        return disponibles;
    }
    
    public ArrayList<Actividad> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Actividad> lista) {
        this.lista = lista;
    }

}
