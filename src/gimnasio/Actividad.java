/*
 * clase para guardar la información de una Actividad del gimnasio
 */
package gimnasio;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author mfontana
 */
public class Actividad implements Serializable {

    private String nombre;
    private int maxPlazas;
    private int inscritos;
    private double precio;

    public Actividad() {
    }

    public Actividad(String nombre, int maxPlazas, double precio) {
        this.nombre = nombre;
        this.maxPlazas = maxPlazas;
        this.precio = precio;
        inscritos = 0;
    }
    
    // Método que indica si la actividad está llena o no
    public boolean isFull() {
        return maxPlazas == inscritos;
    }
    
    // Método que devuelve el nº de plazas disponibles
    public int getPlazasDisponibles() {
        return maxPlazas - inscritos;
    }

    @Override
    public String toString() {
        return "Nombre=" + nombre + ", maxPlazas=" + maxPlazas + ", inscritos=" + inscritos + ", precio=" + precio + '€';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    // Arreglamos el equals para que no sea case sensitive.
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Actividad other = (Actividad) obj;
        return this.nombre.equalsIgnoreCase(other.getNombre());
    }
    
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getInscritos() {
        return inscritos;
    }

    public void setInscritos(int inscritos) {
        this.inscritos = inscritos;
    }

    public int getMaxPlazas() {
        return maxPlazas;
    }

    public void setMaxPlazas(int maxPlazas) {
        this.maxPlazas = maxPlazas;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
