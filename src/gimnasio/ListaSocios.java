/*
 * Clase que encapsula el ArrayList de Socio.
 */
package gimnasio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class ListaSocios implements Serializable {

    private ArrayList<Socio> lista;

    public ListaSocios() {
        lista = new ArrayList<>();
    }

    // Método que añade un Socio a la lista
    public void altaSocio(Socio s) {
        lista.add(s);
    }

    // Método que indica si un Socio existe o no en la lista
    // Utiliza el equals sobreescrito en la clase Socio
    // Dos socios son iguales si tienen el mismo nº de socio
    public boolean existe(Socio s) {
        return lista.contains(s);
    }

    // Método que devuelve la cantidad de socios que hay en la lista
    public int cantidad() {
        return lista.size();
    }

    // Método que a partir del nº de socio me devuelve el Socio
    // Si no existe devuelve null
    public Socio getSocio(int numSocio) {
        for (Socio s : lista) {
            if (s.getNumSocio() == numSocio) {
                return s;
            }
        }
        return null;
    }
    
    // Método que a partir de la posición me devuelva el socio
    public Socio getSocioPosicion(int posicion) {
        return lista.get(posicion);
    }

    // Método que devuelve la suma de todas las cuotas de los socios
    public double cuotasTotales() {
        double total = 0;
        for (Socio s : lista) {
            total += s.getCuota();
        }
        return total;
    }

    public ArrayList<Socio> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Socio> lista) {
        this.lista = lista;
    }

}
